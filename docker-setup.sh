#!/bin/bash
#
# This is to be run on a standalone host that will run the docker engine.
#

DOCKER_REGISTRY="www.rxandyou.com:3625"

aptitude install linux-image-extra-`uname -r`

sh -c "wget -qO- https://get.docker.io/gpg | apt-key add -"

sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D


# sh -c "echo deb http://get.docker.io/ubuntu docker main >/etc/apt/sources.list.d/docker.list"
sh -c "echo deb http://apt.dockerproject.org/repo ubuntu-trusty docker main >>/etc/apt/sources.list.d/docker.list"
aptitude update
aptitude install -y lxc-docker

#
# Setup firewall forwarding to access the containers.
sed -i -e 's/DEFAULT_FORWARD_POLICY="DROP"/DEFAULT_FORWARD_POLICY="ACCEPT"/' /etc/default/ufw
ufw reload

#
# Setup our common data directory for our containers to use
if [ ! -d /data ]; then
    mkdir /data
fi


mydomain=`hostname | awk -F. '{ printf "%s.%s", $2, $3 }'`
PORTS=`dig +short knock._deploy.$mydomain txt | sed -e 's/^"//'  -e 's/"$//'`
docker_pass=`echo $PORTS | md5sum | cut -d' ' -f 1`

docker login --username=rxdeploy --password=$docker_pass \
        --email=webmaster@bioinformatix.io $DOCKER_REGISTRY

